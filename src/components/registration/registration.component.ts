import { Component } from "@angular/core";
import {
  LoginRegistrationManager,
  TokenRegistrationManager
} from "../../cloud-services/registration.service";
import { MyRegistrationManager } from "../../worker/registration.worker";

@Component({
  selector: "zp-registration",
  templateUrl: "./registration.component.html"
})
export class RegistrationComponent {
  private login: string;
  private password: string;
  private passwordConfirmation: string;
  private token: string;

  constructor(
    private loginRegister: LoginRegistrationManager,
    private tokenRegister: TokenRegistrationManager,
    private myRegister: MyRegistrationManager
  ) {}

  register() {
    console.log(`RegistrationComponent::register`);
    const output = this.loginRegister.register({
      login: this.login,
      password: this.password,
      passwordConfirmation: this.passwordConfirmation
    });
    console.log("output", output);
  }

  registerWithToken() {
    console.log(`RegistrationComponent::registerWithToken`);
    const output = this.tokenRegister.register({
      token: this.token
    });
    console.log("output", output);
  }

  customRegister() {
    console.log(`RegistrationComponent::myRegister`);
    const output = this.myRegister.register({
      login: this.login,
      password: this.password,
      passwordConfirmation: this.passwordConfirmation
    });
    console.log("output", output);
  }
}
