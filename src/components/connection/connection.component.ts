import { Component } from "@angular/core";
import {
  LoginAuthenticationManager,
  TokenAuthenticationManager
} from "../../cloud-services/authentication.service";

@Component({
  selector: "zp-connection",
  templateUrl: "./connection.component.html"
})
export class ConnectionComponent {
  private login: string;
  private password: string;
  private token: string;

  constructor(
    private loginAuth: LoginAuthenticationManager,
    private tokenAuth: TokenAuthenticationManager
  ) {}

  connectWithLoginPassword(): void {
    console.log(`Authentication::LoginPasswordConnection`);
    const output = this.loginAuth.login({
      login: this.login,
      password: this.password
    });
    console.log("output", output);
  }

  connectWithToken(): void {
    console.log(`Authentication::TokenAuthentication`);
    const output = this.tokenAuth.login({
      token: this.token
    });
    console.log("output", output);
  }
}
