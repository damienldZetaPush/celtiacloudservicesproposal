import { Component } from "@angular/core";
import {
  PasswordManagerFromEmail,
  PasswordManagerFromLogin
} from "../../cloud-services/reset-password.service";

@Component({
  selector: "zp-reset-password",
  templateUrl: "./reset-password.component.html"
})
export class ResetPasswordComponent {
  private login: string;
  private email: string;

  constructor(
    private pwdManagerEmail: PasswordManagerFromEmail,
    private pwdManagerLogin: PasswordManagerFromLogin
  ) {}

  resetPasswordByEmail() {
    console.log(`ResetPasswordComponent::resetPasswordByEmail`);
    const output = this.pwdManagerEmail.reset({
      email: this.email
    });
    console.log("output", output);
  }

  registerWithToken() {
    console.log(`ResetPasswordComponent::resetPasswordByLogin`);
    const output = this.pwdManagerLogin.reset({
      login: this.login
    });
    console.log("output", output);
  }
}
