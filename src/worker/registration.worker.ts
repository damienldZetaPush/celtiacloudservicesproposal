import {
  LoginRegistrationManager,
  PasswordMinimumLength,
  LoginRegistration,
  EmailValidationRegistration
} from "../cloud-services/registration.service";

/**
 * ===============================
 * Define mandatory fields
 * ===============================
 */
interface MyMandatoryFields extends LoginRegistration {
  // @Format('/myregex$')
  email: string;
  mobilenumber: number;
}

/**
 * ===============================
 * Action on register / validation
 * ===============================
 */
/**
 * Function called each time an user create an account
 * @param context context of the registration with the current user, the date, etc...
 */
function myOnRegisterAccount(context) {}

/**
 * Function called each time an user validate his account
 * @param context context of the registration with the current user, the date, etc...
 */
function myOnValidateAccount(context) {}

// @MinLength({'email': 6})
// @PasswordFormat()
// @Required(['password', 'email'])
// @SearchableFields({'login', 'email'})
// @Uniqueness(['login'])
// @Required(MyMandatoryFields)
// @OnRegisterAccount(myOnRegisterAccount)
// @OnValidateAccount(myOnValidateAccount)
@PasswordMinimumLength(6)
export class MyRegistrationManager extends LoginRegistrationManager {}

/**
 * ===============================
 * Register with validation
 * ===============================
 */
export class myValidationRegistration implements EmailValidationRegistration {
  email = "toto@zetapush.com";
  emailConfiguration = {
    username: "username",
    password: "password",
    host: "host",
    from: "from"
  };
}

// @RegistrationValidationRequired(myValidationRegistration)
export class MyRegistrationManagerWithValidation extends LoginRegistrationManager {}
