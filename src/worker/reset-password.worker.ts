import {
  PasswordManager,
  PasswordManagerFromEmail
} from "../cloud-services/reset-password.service";
import { LoginRegistrationManager } from "../cloud-services/registration.service";

export class myCustomPasswordManager implements PasswordManager {
  reset() {
    // I do what I want
  }
}

// @OnPasswordReset(myCallback)
// @NoValidationRequired() No email sent, the password is automatically reset
export class myCustomPasswordManagerFromEmail extends PasswordManagerFromEmail {}

/**
 * We can use same object for different services
 */

// @MinLength(6)
// @Pattern(/regex$)
interface MyPasswordValidation {}

// @Validation({ 'password': MyPasswordValidation})
export class myCustomPasswordManagerWithPasswordValidation extends PasswordManagerFromEmail {}

// @Validation({ 'password': MyPasswordValidation})
export class MyRegistrationManager extends LoginRegistrationManager {}
