import { LoginAuthenticationManager } from "../cloud-services/authentication.service";
// import { CallUiFunction } from "@zetapush/platform";

/**
 * Function called each time an user is connected
 * @param context context of the authentication with the current user, the date, etc...
 */
function myOnConnectionCallback(context) {}

// @OnConnectionSuccess(myOnConnectionCallback)
// @OnConnectionFailed(myOnConnectionCallback)
export class MyAuthenticationManager extends LoginAuthenticationManager {}

/**
 * Send a notification on the UI when an user is connected
 * @param context context of the connection
 */
function sendNotifOnEachConnection(context) {
  const content = `Hello ${context.user.username} ! Welcome to the dashboard !`;

  // Call a function to the front part (The syntax is not correct)
  // CallUiFunction.call({
  //   name: "displayPopup",
  //   params: {
  //     content
  //   }
  // });
}
