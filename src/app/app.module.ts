import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { ConnectionComponent } from "../components/connection/connection.component";
import { RegistrationComponent } from "../components/registration/registration.component";
import { ResetPasswordComponent } from "../components/reset-password/reset-password.component";

import {
  LoginAuthenticationManager,
  TokenAuthenticationManager
} from "../cloud-services/authentication.service";

import {
  LoginRegistrationManager,
  TokenRegistrationManager
} from "../cloud-services/registration.service";

import { MyRegistrationManager } from "../worker/registration.worker";
import { MyAuthenticationManager } from "../worker/authentication.worker";
import {
  PasswordManagerFromEmail,
  PasswordManagerFromLogin
} from "../cloud-services/reset-password.service";

@NgModule({
  declarations: [
    AppComponent,
    ConnectionComponent,
    RegistrationComponent,
    ResetPasswordComponent
  ],
  imports: [BrowserModule, FormsModule],
  providers: [
    LoginAuthenticationManager,
    TokenAuthenticationManager,
    LoginRegistrationManager,
    TokenRegistrationManager,
    MyRegistrationManager,
    MyAuthenticationManager,
    PasswordManagerFromEmail,
    PasswordManagerFromLogin
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
