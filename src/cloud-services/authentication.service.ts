interface AuthenticationManager {
  login(auth: Authentication);
  logout();
}

interface Authentication {}

interface LoginAuthentication extends Authentication {
  login: string;
  password: string;
}

interface TokenAuthentication extends Authentication {
  token: string;
}

export class LoginAuthenticationManager implements AuthenticationManager {
  login(auth: LoginAuthentication) {
    return auth;
  }
  logout() {
    return true;
  }
}

export class TokenAuthenticationManager implements AuthenticationManager {
  login(auth: TokenAuthentication) {
    return auth;
  }
  logout() {
    return true;
  }
}
