export interface EmailConfiguration {
  username: string;
  password: string;
  host: string;
  port?: number;
  from: string;
  replyTo?: string;
  ssl?: boolean;
  starttls?: boolean;
}
