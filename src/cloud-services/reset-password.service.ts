import { EmailConfiguration } from "./utils";

export interface PasswordManager {
  reset(resetPassword: ResetPassword);
}

interface ResetPassword {}

interface ResetPasswordFromEmail extends ResetPassword {
  email: string;
  emailConfiguration?: EmailConfiguration;
}

interface ResetPasswordFromLogin extends ResetPassword {
  login: string;
  emailConfiguration?: EmailConfiguration;
}

/**
 * By default, the user is identified by email and the new password is
 * sent to this email
 * By default we use the email account of ZetaPush with limitation
 */
export class PasswordManagerFromEmail implements PasswordManager {
  reset(resetPassword: ResetPasswordFromEmail) {
    return true;
  }
}

/**
 * By default, the user is identified by login and the new password is
 * sent to his email (failed if no email exists)
 * By default we use the email account of ZetaPush with limitation
 */
export class PasswordManagerFromLogin implements PasswordManager {
  reset(resetPassword: ResetPasswordFromLogin) {
    return true;
  }
}
