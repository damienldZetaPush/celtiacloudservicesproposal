import { EmailConfiguration } from "./utils";

interface RegistrationManager {
  register(registration: Registration);
}

export interface Registration {}

export interface LoginRegistration extends Registration {
  login: string;
  password: string;
  passwordConfirmation: string;
}

export interface TokenRegistration extends Registration {
  token: string;
}

export class LoginRegistrationManager implements RegistrationManager {
  passwordMinimumLength: number;

  register(registration: LoginRegistration) {
    if (
      this.passwordMinimumLength > 0 &&
      registration.password.length < this.passwordMinimumLength
    ) {
      throw `Password minimum length is ${this.passwordMinimumLength}`;
    } else {
      return registration;
    }
  }
}

export class TokenRegistrationManager implements RegistrationManager {
  register(registration: TokenRegistration) {
    return registration;
  }
}

export function PasswordMinimumLength(value: number) {
  return function(target) {
    Object.defineProperty(target.prototype, "passwordMinimumLength", {
      value: value
    });
  };
}

/**
 * ========================
 * Register validation part
 * ========================
 */
export interface ValidationRegistration {}

export interface EmailValidationRegistration {
  email: string;
  emailConfiguration: EmailConfiguration;
}
